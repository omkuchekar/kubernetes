#Project- With this project we can simply get nginx application from dockerhub image,
#deploy this on kubernetes cluster, connect with mongo db , access it using public ip provided by dockerhub http://192.168.59.101:30100/
#Below setup can be completed in linux/windows
#Prequisite
#1.Docker setup on local machine
#2.Oracle VM provider or similar(Even docker container also fine)
#3.Mongo db will be created inside this container and just need to connect using kubernetes
#4.C:\Users\praja\.kube\config  (This is important file which will be created after hitting minikube start command)

Repository for the K8s in 1 hour video


#K8s manifest files

mongo-config.yaml
mongo-secret.yaml
mongo.yaml
webapp.yaml


#K8s commands

start Minikube and check status

minikube start --vm-driver=hyperkit 
minikube status



#get minikube node's ip address

minikube ip



#get basic info about k8s components

kubectl get node
kubectl get pod
kubectl get svc
kubectl get all



#get extended info about components

kubectl get pod -o wide
kubectl get node -o wide



#get detailed info about a specific component

kubectl describe svc {svc-name}
kubectl describe pod {pod-name}



#get application logs

kubectl logs {pod-name}



#stop your Minikube cluster

minikube stop


⚠ Known issue - Minikube IP not accessible

If you can't access the NodePort service webapp with MinikubeIP:NodePort, execute the following command:

minikube service webapp-service


#Links

mongodb image on Docker Hub: https://hub.docker.com/_/mongo

webapp image on Docker Hub: https://hub.docker.com/repository/docker/nanajanashia/k8s-demo-app

k8s official documentation: https://kubernetes.io/docs/home/

webapp code repo: https://gitlab.com/nanuchi/developing-with-docker/-/tree/feature/k8s-in-hour

****************************************************************************************************


https://www.youtube.com/watch?v=s_o8dwzRlu4&t=103s

Kubernetes, also known as K8s, is the most popular platform for container orchestration for automating deployment, 
scaling, and management of containerized applications.

In this complete Kubernetes Tutorial you will learn everything you need to know to get started with Kubernetes in 1 hour.
 You will learn all the core concepts, including the main Kubernetes components that you need to learn to work efficiently with Kubernetes.
 You will learn the syntax and contents of K8s configuration file , which is used to create and configure components in a 
 Kubernetes cluster as well as how to setup a K8s cluster locally. Finally, I will show a hands-on demo project, 
 where you will learn how to deploy a web application with its database into a local Kubernetes cluster. 
 It's a simple but realistic application setup, which can also serve as a blueprint configuration for most common application setups.


▬▬▬▬▬▬  L I N K S  🔗▬▬▬▬▬▬ 
Git Repo to follow along the demos:  ►  https://gitlab.com/nanuchi/k8s-in-1-hour
Minikube Installation Guides:              ►  https://minikube.sigs.k8s.io/docs/start/


💎  COURSE CONTENTS  💎  ▬▬▬▬▬▬ 
💎 Part 01: Introduction to Kubernetes
               ⌨️  What is Kubernetes
               ⌨️  Benefits of Kubernetes
               ⌨️  Kubernetes Architecture
💎 Part 02: Main Kubernetes Components
                ⌨️  Node & Pod
                ⌨️  Configuring Access with Service & Ingress
                ⌨️  External Configuration with ConfigMap & Secret
                ⌨️  Persisting Data with Volume
                ⌨️  Replication with Deployment & StatefulSet
💎 Part 03: Kubernetes Configuration
💎 Part 04: Setup Kubernetes cluster locally (Minikube and kubectl)
               ⌨️  What is Minikube
               ⌨️  What is Kubectl
               ⌨️  Install Minikube and Kubectl
💎 Part 05: Complete Demo Project: Deploy WebApp with MongoDB
               ⌨️  Demo Project Overview
               ⌨️  Create MongoDB ConfigMap
               ⌨️  Create MongoDB Secret
               ⌨️  Create MongoDB Deployment and Service
               ⌨️  Create WebApp Deployment and Service
               ⌨️  Pass Secret Data to MongoDB Deployment
               ⌨️  Pass Config Data to WebApp Deployment
               ⌨️  Configure External Access
               ⌨️  Deploy all K8s resources into Minikube cluster
               ⌨️  Interacting with Kubernetes Cluster
               ⌨️  Access Web Application in Browser
